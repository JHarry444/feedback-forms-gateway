package com.qa.gateway;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({ GatewayApplicationTests.class, AccountServiceTest.class, CohortsServiceTest.class, FeedbackFormServiceTest.class, AccountBasicTests.class, CohortBasicTests.class, FeedbackFormTraineeBasicTests.class, FeedbackFormTrainerBasicTests.class })

public class TestSuite {
}
