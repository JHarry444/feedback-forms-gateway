package com.qa.gateway;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.qa.gateway.persistence.domain.FeedbackFormTrainee;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FeedbackFormTraineeBasicTests {
	private FeedbackFormTrainee form = new FeedbackFormTrainee();
	
	@Test
	public void formIDTest() {
		form.setTraineeFormID((long) 1);
		assertEquals((long) 1, (long) form.getTraineeFormID());
	}
	
	@Test
	public void CohortIDTest() {
		form.setCohortID((long) 1);
		assertEquals((long) 1, (long) form.getCohortID());
	}
	
	@Test
	public void trainerIDTest() {
		form.setTrainerID((long) 1);
		assertEquals((long) 1, (long) form.getTrainerID());
	}
	
	@Test
	public void traineeIDTest() {
		form.setTraineeID((long) 1);
		assertEquals((long) 1, (long) form.getTraineeID());
	}
	
	@Test
	public void scoreTest() {
		form.setScore(1);
		assertEquals(1, form.getScore());
	}
	
	@Test
	public void answersTest() {
		String[] ans = {"No", "thanks"};
		form.setAnswers(ans);
		assertArrayEquals(ans, form.getAnswers());
	}
	
	@Test
	public void countTest() {
		form.setFormCount(1);
		assertEquals(1, form.getFormCount());
	}
}
