package com.qa.gateway;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.qa.gateway.persistence.domain.Cohort;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CohortBasicTests {
	private Cohort cohort = new Cohort();
	
	@Test
	public void cohortIDTest() {
		cohort.setCohortID((long) 1);
		assertEquals((long) 1, (long) cohort.getCohortID());
	}
	
	@Test
	public void cohortNameTest() {
		cohort.setCohortName("Decembuary 1884");
		assertTrue(cohort.getCohortName().contentEquals("Decembuary 1884"));
	}
	
	@Test
	public void trainerNameTest() {
		cohort.setTrainerName("John Smith");
		assertTrue(cohort.getTrainerName().contentEquals("John Smith"));
	}
	
//	@Test
//	public void weekTest() {
//		cohort.setWeek(1);
//		assertEquals(1, cohort.getWeek());
//	}
	
	@Test
	public void descriptionTest() {
		cohort.setCohortDescription("Not very good.");
		assertTrue(cohort.getCohortDescription().contentEquals("Not very good."));
	}

}
