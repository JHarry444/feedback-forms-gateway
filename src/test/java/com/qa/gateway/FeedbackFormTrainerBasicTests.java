package com.qa.gateway;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.qa.gateway.persistence.domain.FeedbackFormTrainer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FeedbackFormTrainerBasicTests {
	private FeedbackFormTrainer form = new FeedbackFormTrainer();
	
	@Test
	public void formIDTest() {
		form.setTrainerFormID((long) 1);
		assertEquals((long) 1, (long) form.getTrainerFormID());
	}
	
	@Test
	public void trainerIDTest() {
		form.setTrainerID((long) 1);
		assertEquals((long) 1, (long) form.getTrainerID());
	}
	
	@Test
	public void traineeIDTest() {
		form.setTraineeID((long) 1);
		assertEquals((long) 1, (long) form.getTraineeID());
	}
	
	@Test
	public void scoresTest() {
		int[] scrs = {1, 2};
		form.setScores(scrs);
		assertEquals(scrs, form.getScores());
	}
	
	@Test
	public void answersTest() {
		String[] ans = {"No", "thanks"};
		form.setAnswers(ans);
		assertArrayEquals(ans, form.getAnswers());
	}
	
	@Test
	public void commentsTest() {
		form.setCommentsForTrainee("He's awful");
		assertTrue(form.getCommentsForTrainee().contentEquals("He's awful"));
	}
}
