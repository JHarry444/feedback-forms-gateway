package com.qa.gateway;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jms.core.JmsTemplate;

import com.qa.gateway.persistence.domain.Cohort;
import com.qa.gateway.persistence.repository.CohortRepository;
import com.qa.gateway.service.CohortServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CohortsServiceTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@InjectMocks
	private CohortServiceImpl service;

	@Mock
	private CohortRepository repo;
	
	@Mock
	private JmsTemplate jms;

	private static final Cohort MOCK_COHORT_1 = new Cohort("December 2018", "Mathew Hunt", 4, "Introduction to Java EE");
	private static final Cohort MOCK_COHORT_2 = new Cohort("January 2019", "Jordan Harrison", 4, "Introduction to DevOps");

	@Test
	public void addCohortTest() {
//		Mockito.when(repo.save(MOCK_COHORT_1)).thenReturn(MOCK_COHORT_1);
		assertEquals(MOCK_COHORT_1, service.addCohort(MOCK_COHORT_1));
//		Mockito.verify(repo).save(MOCK_COHORT_1);
	}
	
	@Test
	public void findIDTest() {
		Cohort cohort = new Cohort();
		cohort.setCohortID((long) 1);
		Mockito.when(repo.findByCohortID((long) 1)).thenReturn(cohort);
		assertEquals(cohort, service.findByCohortID((long) 1));
	}
	
	@Test
	public void deleteIDTest() {
		Mockito.when(repo.deleteByCohortID((long) 1)).thenReturn((long) 1);
		assertEquals((long) 1, (long) service.deleteByCohortID((long) 1));
	}
	
	@Test
	public void deleteNameTest() {
		Mockito.when(repo.deleteByCohortName("December 2018")).thenReturn((long) 1);
		assertEquals((long) 1, (long) service.deleteByCohortName("December 2018"));
	}
	
	@Test
	public void findAllTest() {
		List listing = new ArrayList<Cohort>();
		listing.add(MOCK_COHORT_1);
		listing.add(MOCK_COHORT_2);
		Mockito.when(repo.findAll()).thenReturn(listing);
		assertEquals(listing, service.findCohorts());
	}

}
