package com.qa.gateway;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jms.core.JmsTemplate;

import com.qa.gateway.persistence.domain.FeedbackFormTrainee;
import com.qa.gateway.persistence.domain.FeedbackFormTrainer;
import com.qa.gateway.persistence.repository.FeedbackFormTraineeRepository;
import com.qa.gateway.persistence.repository.FeedbackFormTrainerRepository;
import com.qa.gateway.service.FeedbackFormService;

@RunWith(MockitoJUnitRunner.class)
public class FeedbackFormServiceTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@InjectMocks
	private FeedbackFormService service;

	@Mock
	private FeedbackFormTraineeRepository traineeRepo;
	
	@Mock
	private FeedbackFormTrainerRepository trainerRepo;
	
	@Mock
	private JmsTemplate jms;
	
	private FeedbackFormTrainee traineeForm = new FeedbackFormTrainee();
	private FeedbackFormTrainer trainerForm = new FeedbackFormTrainer();
	private static final String[] ANSWERS = {"No", "way"};
	private static final FeedbackFormTrainee MOCK_TRAINEE_FORM = new FeedbackFormTrainee((long) 1, 2, 3, ANSWERS);
	private static final int[] SCORES = {1, 2, 3};
	private static final FeedbackFormTrainer MOCK_TRAINER_FORM = new FeedbackFormTrainer((long) 1, (long) 3, SCORES, ANSWERS, "Not very good.");
	@Test
	public void addTraineeTest() {
		assertEquals(MOCK_TRAINEE_FORM, service.createTraineeForm(MOCK_TRAINEE_FORM));
	}
	
	@Test
	public void addTrainerTest() {
		assertEquals(MOCK_TRAINER_FORM, service.createTrainerForm(MOCK_TRAINER_FORM));
	}
	
	@Test
	public void traineeFormIDTest() {
		
		traineeForm.setTraineeFormID((long) 1);
		Mockito.when(traineeRepo.findByTraineeFormID((long) 1)).thenReturn(traineeForm);
		assertEquals(traineeForm, service.findTraineeForm((long) 1));
	}
	
	@Test
	public void trainerFormIDTest() {
		trainerForm.setTrainerFormID((long) 1);
		Mockito.when(trainerRepo.findByTrainerFormID((long) 1)).thenReturn(trainerForm);
		assertEquals(trainerForm, service.findTrainerForm((long) 1));
	}
	
	@Test
	public void traineeTraineeIDTest() {
		traineeForm.setTraineeID((long) 1);
		List<FeedbackFormTrainee> listing = new ArrayList<FeedbackFormTrainee>();
		listing.add(traineeForm);
		Mockito.when(traineeRepo.findAllByTraineeID((long) 1)).thenReturn(listing);
		assertEquals(listing, service.findTraineeFormsByTraineeID((long) 1));
	}
	
	@Test
	public void trainerTraineeIDTest() {
		trainerForm.setTraineeID((long) 1);
		List<FeedbackFormTrainer> listing = new ArrayList<FeedbackFormTrainer>();
		listing.add(trainerForm);
		Mockito.when(trainerRepo.findAllByTraineeID((long) 1)).thenReturn(listing);
		assertEquals(listing, service.findTrainerFormsByTraineeID((long) 1));
	}
	
	@Test
	public void traineeCohortIDTest() {
		traineeForm.setCohortID((long) 1);
		List<FeedbackFormTrainee> listing = new ArrayList<FeedbackFormTrainee>();
		listing.add(traineeForm);
		Mockito.when(traineeRepo.findAllByCohortID((long) 1)).thenReturn(listing);
		assertEquals(listing, service.findTraineeFormsByCohortID((long) 1));
	}
	
	@Test
	public void traineeDeleteTraineeIDTest() {
		Mockito.when(traineeRepo.deleteAllByTraineeID((long) 1)).thenReturn((long) 1);
		assertEquals((long) 1, (long) service.deleteTraineeFormsByTraineeID((long) 1));
	}
	
	@Test
	public void trainerDeleteTraineeIDTest() {
		Mockito.when(trainerRepo.deleteAllByTraineeID((long) 1)).thenReturn((long) 1);
		assertEquals((long) 1, (long) service.deleteTrainerFormsByTraineeID((long) 1));
	}
	
	@Test
	public void traineeDeleteCohortIDTest() {
		Mockito.when(traineeRepo.deleteAllByCohortID((long) 1)).thenReturn((long) 1);
		assertEquals((long) 1, (long) service.deleteTraineeFormsByCohortID((long) 1));
	}
	
	
	
}