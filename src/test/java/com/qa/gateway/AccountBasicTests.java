package com.qa.gateway;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.qa.gateway.persistence.domain.Account;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountBasicTests {
	private Account account = new Account();
	
	@Test
	public void accountIDTest() {
		account.setAccountID((long) 1);
		assertEquals((long) 1, (long) account.getAccountID());
	}
	
	@Test
	public void cohortIDTest() {
		account.setCohortID((long) 1);
		assertEquals((long) 1, (long) account.getCohortID());
	}
	
	@Test
	public void lastNameTest() {
		account.setLastName("Smith");
		assertTrue(account.getLastName().contentEquals("Smith"));
	}
	
	@Test
	public void emailTest() {
		account.setEmail("soandso@qa.com");
		assertTrue(account.getEmail().contentEquals("soandso@qa.com"));
	}
	
	@Test
	public void passwordTest() {
		account.setPassword("swordfish");
		assertTrue(account.getPassword().contentEquals("swordfish"));
	}
	
	@Test
	public void flagTest() {
		account.setFlagged(true);
		assertTrue(account.isFlagged());
	}

}
