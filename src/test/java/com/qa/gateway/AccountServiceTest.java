package com.qa.gateway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Arrays;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;

import com.qa.gateway.persistence.domain.Account;
import com.qa.gateway.persistence.repository.AccountRepository;
import com.qa.gateway.service.AccountServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AccountServiceTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@InjectMocks
	private AccountServiceImpl service;

	@Mock
	private AccountRepository repo;
	
	@Mock
	private JmsTemplate jms;

	private static final Account MOCK_ACCOUNT_1 = new Account((long) 5, "trainee", "Taylor", "White", "TaylorWhite@qa.com", "PASS1", false);
	private static final Account MOCK_ACCOUNT_2 = new Account((long) 6, "trainer", "Joseph", "Brown", "JosephBrown@qa.com", "", false);
	private static final Account MOCK_ACCOUNT_3 = new Account((long) 7, "trainee", "Nell", "Count", "NellCount@gmail.com", "PASSWORD1", false);
	private static final Account MOCK_ACCOUNT_4 = new Account((long) 8, "trainee", "Tom", "Cat", "TomCat@academytrainee.com", "PASSWORD2", false);
	private static final Account MOCK_ACCOUNT_5 = new Account((long) 9, "trainer", "Ned", "Trenner", "NedTrenner@academytrainee.com", "PASSWORD2", false);

	private static final Optional<Account> MOCK_ACCOUNT_OPTIONAL = Optional.of(MOCK_ACCOUNT_1);
	private static final Optional<Account> MOCK_NULL_OPTIONAL = Optional.empty();
	private static final ResponseEntity<Object> MOCK_OK_RESPONSE = new ResponseEntity<Object>(HttpStatus.OK);
	private static final ResponseEntity<Object> MOCK_NOT_FOUND_RESPONSE = new ResponseEntity<Object>(HttpStatus.NOT_FOUND);

	@Test
	public void addTrainerAccountTest() {
		Mockito.when(repo.save(MOCK_ACCOUNT_1)).thenReturn(MOCK_ACCOUNT_1);
		assertEquals(MOCK_ACCOUNT_1, service.addAccount(MOCK_ACCOUNT_1));
//		Mockito.verify(repo).save(MOCK_ACCOUNT_1);
	}

	@Test
	public void addTrainerAccountCheckAdminTrueTest() {
		Mockito.when(repo.save(MOCK_ACCOUNT_2)).thenReturn(MOCK_ACCOUNT_2);
		assertEquals("trainer", MOCK_ACCOUNT_2.getType());
	}

	@Test
	public void addInvalidAccountTest() {
		Mockito.when(repo.save(MOCK_ACCOUNT_3)).thenReturn(MOCK_ACCOUNT_3);
		assertEquals(null, service.addAccount(MOCK_ACCOUNT_3));
	}

	@Test
	public void addTraineeAccountTest() {
		Mockito.when(repo.save(MOCK_ACCOUNT_4)).thenReturn(MOCK_ACCOUNT_4);
		assertEquals(MOCK_ACCOUNT_4, service.addAccount(MOCK_ACCOUNT_4));
	}

	@Test
	public void addTraineeAccountCheckAdminFalseTest() {
		Mockito.when(repo.save(MOCK_ACCOUNT_4)).thenReturn(MOCK_ACCOUNT_4);
		assertEquals("trainee", MOCK_ACCOUNT_4.getType());
	}
	
	@Test
	public void findCohortIDTest() {
		List result = new ArrayList<Account>();
		result.add(MOCK_ACCOUNT_1);
		Mockito.when((repo.findAllByCohortID((long) 5))).thenReturn(result);
		assertEquals(MOCK_ACCOUNT_1, service.findAllByCohortID((long) 5).get(0));		
	}
	
	@Test
	public void findAccountIDTest() {
		Account account = new Account();
		account.setAccountID((long) 1);
		account.setFirstName("John");
		Mockito.when(repo.findByAccountID((long) 1)).thenReturn(account);
		assertEquals("John", service.findByAccountID((long) 1).getFirstName());
	}
	
	@Test
	public void findEmailTest() {
		Mockito.when(repo.findByEmail("TaylorWhite@qa.com")).thenReturn(MOCK_ACCOUNT_1);
		assertEquals(MOCK_ACCOUNT_1, service.findByEmail("TaylorWhite@qa.com"));
	}
	
	@Test
	public void findAllTest() {
		List listing = new ArrayList<Account>();
		listing.add(MOCK_ACCOUNT_1);
		listing.add(MOCK_ACCOUNT_2);
		Mockito.when(repo.findAll()).thenReturn(listing);
		assertEquals(listing, service.findAll());
	}
	
	@Test
	public void successfullyDeleteAccount() {
		Mockito.when(repo.findByEmail("TaylorWhite@qa.com")).thenReturn(MOCK_ACCOUNT_1);
		Mockito.when(repo.deleteByEmail("TaylorWhite@qa.com")).thenReturn(1L);
		assertEquals(1L, (long) service.deleteByEmail("TaylorWhite@qa.com"));
	}
	
	@Test
	public void failToDeleteAccount() {
		Mockito.when(repo.findByEmail("JosephBrown@qa.com")).thenReturn(null);
		assertEquals(-1L, (long) service.deleteByEmail("JosephBrown@qa.com"));
	}
	
	@Test
	public void manualAdminTest() {
		Mockito.when(repo.save(MOCK_ACCOUNT_5)).thenReturn(MOCK_ACCOUNT_5);
		assertEquals(MOCK_ACCOUNT_5.getType(), "trainer");
	}
	


}