package com.qa.gateway.rest;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.qa.gateway.persistence.domain.Account;
import com.qa.gateway.service.AccountService;

@CrossOrigin
@RequestMapping("${path.base}")
@RestController
public class AccountRest {

	private AccountService src;
	private RestTemplate rest;

	public AccountRest(AccountService src, RestTemplate rest) {
		this.rest = rest;
		this.src = src;
	}

	@PostMapping("${path.addAccount}")
	public Account addAccount(@RequestBody Account account) {
		return src.addAccount(account);
	}

	@GetMapping("${path.getAccounts}")
	public List<Account> getAccounts() {
		return src.findAll();
	}

	@GetMapping("${path.getAccountsByCohortID}")
	public List<Account> getAccountsByCohortID(@PathVariable Long id) {
		return src.findAllByCohortID(id);
	}

	@GetMapping("${path.getAccountByAccountID}")
	public Account getAccountByAccountID(@PathVariable Long id) {
		return src.findByAccountID(id);
	}

	@GetMapping("${path.getAccountByEmail}")
	public Account getAccountByEmail(@PathVariable String email) {
		return src.findByEmail(email);
	}
	
	@PutMapping("${path.updateAccount}")
	public Account updateAccountById(@RequestBody Account account, @PathVariable Long id) {
		return src.updateAccountById(account, id);
	}
	
	@DeleteMapping("${path.deleteByEmail}")
	public Long deleteByEmail(@PathVariable String email) {
		return src.deleteByEmail(email);
	}
	
	@PostMapping("logIn")
	public Account logIn(@RequestBody Account account) {
		return src.logIn(account);
	}

}
