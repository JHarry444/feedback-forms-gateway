package com.qa.gateway.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.qa.gateway.persistence.domain.Email;

@CrossOrigin
@RequestMapping("${path.base}")
@RestController
public class EmailRest {
	
	private RestTemplate rest;
	
	public EmailRest(RestTemplate rest) {
		this.rest = rest;
	}
	
	@PostMapping("sendEmail")
	public void sendEmail(@RequestBody Email email) {
		rest.exchange("http://mail:8080/send", HttpMethod.POST, new HttpEntity<Email>(email), void.class);
	}
	
}
