package com.qa.gateway.rest;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.qa.gateway.persistence.domain.FeedbackFormTrainee;
import com.qa.gateway.persistence.domain.FeedbackFormTrainer;
import com.qa.gateway.service.FeedbackFormService;

@CrossOrigin
@RequestMapping("${path.base}")
@RestController
public class FeedbackFormRest {

	private FeedbackFormService src;
	private RestTemplate rest;

	public FeedbackFormRest(FeedbackFormService src, RestTemplate rest) {
		this.src = src;
		this.rest = rest;
	}

	// Trainee

	@PostMapping("${path.createTraineeForm}")
	public FeedbackFormTrainee createTraineeForm(@RequestBody FeedbackFormTrainee form) {
		return src.createTraineeForm(form);
	}

	@GetMapping("${path.findTraineeForm}")
	public FeedbackFormTrainee findTraineeForm(@PathVariable Long formID) {
		return src.findTraineeForm(formID);
	}
	
	@GetMapping("${path.findAllTraineeForms}")
	public List<FeedbackFormTrainee> findAllTraineeForms() {
		return src.findAllTraineeForms();
	}

	@GetMapping("${path.findTraineeFormsByTraineeID}")
	public List<FeedbackFormTrainee> findTraineeFormsByTraineeID(@PathVariable Long traineeID) {
		return src.findTraineeFormsByTraineeID(traineeID);
	}

	@GetMapping("${path.findTraineeFormsByCohortID}")
	public List<FeedbackFormTrainee> findTraineeFormsByCohortID(@PathVariable Long cohortID) {
		return src.findTraineeFormsByCohortID(cohortID);
	}

	@DeleteMapping("${path.deleteTraineeFormsByTraineeID}")
	public Long deleteTraineeFormsByTraineeID(@PathVariable Long traineeID) {
		return src.deleteTraineeFormsByTraineeID(traineeID);
	}

	@DeleteMapping("${path.deleteTraineeFormsByCohortID}")
	public Long deleteTraineeFormsByCohortID(@PathVariable Long cohortID) {
		return src.deleteTraineeFormsByCohortID(cohortID);
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping("getAveragesForCohortID/{id}")
	public List<Double> getAverages(@PathVariable Long id) {
		List<FeedbackFormTrainee> list = src.findTraineeFormsByCohortID(id);
		return rest.exchange("http://average:8080/getValue", HttpMethod.POST, new HttpEntity<List<FeedbackFormTrainee>>(list), List.class).getBody();
	}

	// Trainer

	@PostMapping("${path.createTrainerForm}")
	public FeedbackFormTrainer createTrainerForm(@RequestBody FeedbackFormTrainer form) {
		return src.createTrainerForm(form);
	}

	@GetMapping("${path.findTrainerForm}")
	public FeedbackFormTrainer findTrainerForm(@PathVariable Long formID) {
		return src.findTrainerForm(formID);
	}
	
	@GetMapping("${path.findAllTrainerForms}")
	public List<FeedbackFormTrainer> findAllTrainerForms() {
		return src.findAllTrainerForms();
	}

	@GetMapping("${path.findTrainerFormsByTraineeID}")
	public List<FeedbackFormTrainer> findTrainerFormsByTraineeID(@PathVariable Long traineeID) {
		return src.findTrainerFormsByTraineeID(traineeID);
	}

	@DeleteMapping("${path.deleteTrainerFormsByTraineeID}")
	public Long deleteTrainerFormsByTraineeID(@PathVariable Long traineeID) {
		return src.deleteTrainerFormsByTraineeID(traineeID);
	}
	
}
