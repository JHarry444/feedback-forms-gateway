package com.qa.gateway.rest;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.qa.gateway.persistence.domain.Cohort;
import com.qa.gateway.service.CohortService;

@CrossOrigin
@RequestMapping("${path.base}")
@RestController
public class CohortRest {

	private CohortService src;
	private RestTemplate rest;

	public CohortRest(CohortService src, RestTemplate rest) {
		this.rest = rest;
		this.src = src;
	}
	
	@PostMapping("${path.addCohort}")
	public Cohort addCohort(@RequestBody Cohort cohort) {
		return src.addCohort(cohort);
	}
	
	@GetMapping("${path.getCohorts}")
	public List<Cohort> getCohorts() {
		return src.findCohorts();
	}

	@GetMapping("${path.getCohortbyID}")
	public Cohort getCohortByID(@PathVariable Long cohortID) {
		return src.findByCohortID(cohortID);
	}
	
	@DeleteMapping("${path.deleteByCohortName}")
	public Long deleteByCohortName(@PathVariable String cohortName) {
		return src.deleteByCohortName(cohortName);
	}
	
	@DeleteMapping("${path.deleteByCohortID}")
	public Long deleteByCohortID(@PathVariable Long cohortID) {
		return src.deleteByCohortID(cohortID);
	}
	
	@PutMapping("updateCount/{cohortID}")
	public Cohort updateCount(@PathVariable Long cohortID) {
		return src.updateCount(cohortID);
	}
}
