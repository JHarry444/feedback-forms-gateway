package com.qa.gateway.service;

import java.util.List;
import java.util.Optional;

import com.qa.gateway.persistence.domain.Account;

public interface AccountService {

	Account addAccount(Account account);
	
	List<Account> findAll();
	
	List<Account> findAllByCohortID(Long id);

	Account findByAccountID(Long id);

	Account findByEmail(String email);
	
	Account updateAccountById(Account account, Long id);

	Long deleteByEmail(String email);

	Account logIn(Account account);

}
