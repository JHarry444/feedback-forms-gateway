package com.qa.gateway.service;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.qa.gateway.persistence.domain.FeedbackFormTrainee;
import com.qa.gateway.persistence.domain.FeedbackFormTrainer;
import com.qa.gateway.persistence.repository.FeedbackFormTraineeRepository;
import com.qa.gateway.persistence.repository.FeedbackFormTrainerRepository;

@Service
public class FeedbackFormService {

	private FeedbackFormTrainerRepository trainerRepo;
	private FeedbackFormTraineeRepository traineeRepo;
	private JmsTemplate jms;

	public FeedbackFormService(FeedbackFormTrainerRepository trainerRepo, FeedbackFormTraineeRepository traineeRepo, JmsTemplate jms) {
		this.traineeRepo = traineeRepo;
		this.trainerRepo = trainerRepo;
		this.jms = jms;
	}

	// Trainee

	public FeedbackFormTrainee createTraineeForm(FeedbackFormTrainee form) {
		jms.convertAndSend("TraineeForms", form);
		return form;
	}

	public FeedbackFormTrainee findTraineeForm(Long formID) {
		return traineeRepo.findByTraineeFormID(formID);
	}

	public List<FeedbackFormTrainee> findTraineeFormsByTraineeID(Long traineeID) {
		return traineeRepo.findAllByTraineeID(traineeID);
	}

	public List<FeedbackFormTrainee> findTraineeFormsByCohortID(Long cohortID) {
		return traineeRepo.findAllByCohortID(cohortID);
	}

	public Long deleteTraineeFormsByTraineeID(Long traineeID) {
		return traineeRepo.deleteAllByTraineeID(traineeID);
	}

	public Long deleteTraineeFormsByCohortID(Long cohortID) {
		return traineeRepo.deleteAllByCohortID(cohortID);
	}
	

	public List<FeedbackFormTrainee> findAllTraineeForms() {
		return traineeRepo.findAll();
	}

	// Trainer

	public FeedbackFormTrainer createTrainerForm(FeedbackFormTrainer form) {
		form.setAverageScore((form.getScores()[0] + form.getScores()[1] + form.getScores()[2])/3);
		// if commentsForTrainee is not null then call email microservice
		jms.convertAndSend("TrainerForms", form);
		return form;
	}

	public FeedbackFormTrainer findTrainerForm(Long formID) {
		return trainerRepo.findByTrainerFormID(formID);
	}

	public List<FeedbackFormTrainer> findTrainerFormsByTraineeID(Long traineeID) {
		return trainerRepo.findAllByTraineeID(traineeID);
	}

	public Long deleteTrainerFormsByTraineeID(Long traineeID) {
		return trainerRepo.deleteAllByTraineeID(traineeID);
	}

	public List<FeedbackFormTrainer> findAllTrainerForms() {
		return trainerRepo.findAll();
	}

}
