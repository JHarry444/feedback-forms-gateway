
package com.qa.gateway.service;

import java.util.List;

import com.qa.gateway.persistence.domain.Cohort;

public interface CohortService {

	Cohort addCohort(Cohort cohort);

	List<Cohort> findCohorts();

	Cohort findByCohortID(Long cohortID);

	Long deleteByCohortName(String cohortName);

	Long deleteByCohortID(Long cohortID);

	Cohort updateCount(Long cohortID);

}
