package com.qa.gateway.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.qa.gateway.persistence.domain.Account;
import com.qa.gateway.persistence.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {

	private AccountRepository repo;
	private JmsTemplate jms;

	public AccountServiceImpl(AccountRepository repo, JmsTemplate jms) {
		this.repo = repo;
		this.jms = jms;
	}

	@Override
	public Account addAccount(Account account) {
		if (account.getEmail().endsWith("qa.com") || account.getEmail().endsWith("academytrainee.com")) {
			if (account.getEmail().endsWith("qa.com")) {
				account.setType("trainer");
			}
			else {
				account.setType("trainee");
			}
			if (repo.findByEmail(account.getEmail()) == null) {
				jms.convertAndSend("AccountQueue", account);
				return account;
			}
			else {
				return null;
			}
		}
		return null;
	}

	@Override
	public List<Account> findAllByCohortID(Long id) {
		return repo.findAllByCohortID(id);
	}

	@Override
	public Account findByAccountID(Long id) {
		return repo.findByAccountID(id);
	}

	@Override
	public Account findByEmail(String email) {
		return repo.findByEmail(email);
	}

	@Override
	public Long deleteByEmail(String email) {
		if (repo.findByEmail(email) != null) {
			return repo.deleteByEmail(email);
		}
		return -1L;
	}

	@Override
	public List<Account> findAll() {
		return repo.findAll();
	}

	@Override
	public Account updateAccountById(Account account, Long id) {
		Account account2 = repo.findByAccountID(id);
		if (account != null) {
			if (account.getCohortID() != null) {
				account2.setCohortID(account.getCohortID());
			}
			if (account.getType() != null) {
				account2.setType(account.getType());
			}
			if (account.getFirstName() != null) {
				account2.setFirstName(account.getFirstName());
			}
			if (account.getLastName() != null) {
				account2.setLastName(account.getLastName());
			}
			if (account.getPassword() != null) {
				account2.setPassword(account.getPassword());
			}
			if (account.isFlagged() != true) {
				account2.setFlagged(true);
			}
		}
		repo.deleteByAccountID(id);
		repo.save(account2);
		return account2;
	}

	@Override
	public Account logIn(Account account) {
		if (repo.findByEmail(account.getEmail()) != null && repo.findByEmail(account.getEmail()).getPassword() == account.getPassword()) {
			Account account2 = repo.findByEmail(account.getEmail());
			account2.setPassword(null);
			return account2;
		}
		return null;
	}

}
