package com.qa.gateway.service;

import java.util.List;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.qa.gateway.persistence.domain.Cohort;
import com.qa.gateway.persistence.repository.CohortRepository;

@Service
public class CohortServiceImpl implements CohortService {

	private CohortRepository repo;
	private JmsTemplate jms;
	
	public CohortServiceImpl(CohortRepository repo, JmsTemplate jms) {
		this.repo = repo;
		this.jms = jms;
	}

	@Override
	public Cohort addCohort(Cohort cohort) {
		jms.convertAndSend("CohortQueue", cohort);
		return cohort;
	}

	@Override
	public Cohort findByCohortID(Long id) {
		return repo.findByCohortID(id);
	}

	@Override
	public Long deleteByCohortName(String cohortName) {
		return repo.deleteByCohortName(cohortName);
	}

	@Override
	public Long deleteByCohortID(Long cohortID) {
		return repo.deleteByCohortID(cohortID);
	}

	@Override
	public List<Cohort> findCohorts() {
		return repo.findAll();
	}
	
	@Override
	public Cohort updateCount(Long cohortID) {
		Cohort cohort = repo.findByCohortID(cohortID);
		repo.deleteByCohortID(cohortID);
		cohort.setFormCount(cohort.getFormCount() + 1);
		repo.save(cohort);
		return cohort;
	}

}
