package com.qa.gateway.persistence.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.qa.gateway.persistence.domain.Account;

@Repository
public interface AccountRepository extends MongoRepository<Account, Long> {

	List<Account> findAllByCohortID(Long id);

	Account findByAccountID(Long id);

	Account findByEmail(String email);

	Long deleteByEmail(String email);
	
	Long deleteByAccountID(Long id);

}
