package com.qa.gateway.persistence.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.qa.gateway.persistence.domain.Cohort;

@Repository
public interface CohortRepository extends MongoRepository<Cohort, Long> {

	Cohort findByCohortID(Long id);

	Long deleteByCohortName(String cohortName);

	Long deleteByCohortID(Long cohortID);

}