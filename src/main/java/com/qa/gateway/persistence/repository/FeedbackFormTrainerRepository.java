package com.qa.gateway.persistence.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.qa.gateway.persistence.domain.FeedbackFormTrainer;

@Repository
public interface FeedbackFormTrainerRepository extends MongoRepository<FeedbackFormTrainer, Long>{
	
	public List<FeedbackFormTrainer> findAllByTraineeID(Long traineeID);
	
	public Long deleteAllByTraineeID(Long traineeID);
	
	public FeedbackFormTrainer findByTrainerFormID(Long trainerFormID);

}
