package com.qa.gateway.persistence.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.qa.gateway.persistence.domain.FeedbackFormTrainee;

@Repository
public interface FeedbackFormTraineeRepository extends MongoRepository<FeedbackFormTrainee, Long>{

	public List<FeedbackFormTrainee> findAllByTraineeID(Long traineeID);
	
	public Long deleteAllByTraineeID(Long traineeID);
	
	public List<FeedbackFormTrainee> findAllByCohortID(Long cohortID);
	
	public Long deleteAllByCohortID(Long cohortID);
	
	public FeedbackFormTrainee findByTraineeFormID(Long traineeFormID);
	
}
